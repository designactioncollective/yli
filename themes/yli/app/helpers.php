<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}


/* Get YouTube ID from string */
function get_youtube_id($text) {
	if (is_numeric(basename($text))) {
		$text = basename($text);
	} else {
		$text = preg_replace('~(?#!js YouTubeId Rev:20160125_1800)
			# Match non-linked youtube URL in the wild. (Rev:20130823)
			https?://          # Required scheme. Either http or https.
			(?:[0-9A-Z-]+\.)?  # Optional subdomain.
			(?:                # Group host alternatives.
			  youtu\.be/       # Either youtu.be,
			| youtube          # or youtube.com or
			  (?:-nocookie)?   # youtube-nocookie.com
			  \.com            # followed by
			  \S*?             # Allow anything up to VIDEO_ID,
			  [^\w\s-]         # but char before ID is non-ID char.
			)                  # End host alternatives.
			([\w-]{11})        # $1: VIDEO_ID is exactly 11 chars.
			(?=[^\w-]|$)       # Assert next char is non-ID or EOS.
			(?!                # Assert URL is not pre-linked.
			  [?=&+%\w.-]*     # Allow URL (query) remainder.
			  (?:              # Group pre-linked alternatives.
				[\'"][^<>]*>   # Either inside a start tag,
			  | </a>           # or inside <a> element text contents.
			  )                # End recognized pre-linked alts.
			)                  # End negative lookahead assertion.
			[?=&+%\w.-]*       # Consume any URL (query) remainder.
			~ix', '$1',
			$text);
    }
    return $text;
}
function get_vimeo_id( $url ) {
	$regex = '~
		# Match Vimeo link and embed code
		(?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
		(?:                             # Group vimeo url
				https?:\/\/             # Either http or https
				(?:[\w]+\.)*            # Optional subdomains
				vimeo\.com              # Match vimeo.com
				(?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
				\/                      # Slash before Id
				([0-9]+)                # $1: VIDEO_ID is numeric
				[^\s]*                  # Not a space
		)                               # End group
		"?                              # Match end quote if part of src
		(?:[^>]*></iframe>)?            # Match the end of the iframe
		(?:<p>.*</p>)?                  # Match any title information stuff
		~ix';

	preg_match( $regex, $url, $matches );

	return $matches[1];
}


/* Featured Video Function */
function the_featured_video($quality = 'hqdefault') {
	$video_source = get_field('video_source');
	$video_url = get_field('choose_video', false, false);
	$play_button = '<button type="button" class="play-button"><span class="sr-only">Play</span><i class="fas fa-play"></i></button>';

	if ('youtube' === $video_source) {
		$youtube_id = get_youtube_id($video_url);

		echo '<div class="vid-container">
				<div class="vid-wrap embed-responsive embed-responsive-16by9"
					 data-video="https://www.youtube.com/embed/' . $youtube_id . '"
					 style="background-image:url(\'https://img.youtube.com/vi/' . $youtube_id . '/'.$quality.'.jpg\');">
					 '.$play_button.'
				</div>
			 </div>';
	} elseif ('vimeo' === $video_source) {
		$vimeo_id = get_vimeo_id($video_url);
		$hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/$vimeo_id.php"));
		$thumbnail = $hash[0]['thumbnail_large'];

		echo '<div class="vid-container">
				<div class="vid-wrap embed-responsive embed-responsive-16by9"
					 data-video="//player.vimeo.com/video/' . $vimeo_id . '"
					 style="background-image:url(\'' . $thumbnail . '\');">
					 '.$play_button.'
				</div>
			 </div>';
	}
}

/*
 * custom pagination with bootstrap .pagination class
 * source: http://www.ordinarycoder.com/paginate_links-class-ul-li-bootstrap/
 */
function bootstrap_pagination( $echo = true ) {
  global $wp_query;

  $big = 999999999; // need an unlikely integer

  $pages = paginate_links( array(
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format' => '?paged=%#%',
      'current' => max( 1, get_query_var('paged') ),
      'total' => $wp_query->max_num_pages,
      'type'  => 'array',
      'prev_next'   => true,
      'prev_text'    => __('Previous'),
      'next_text'    => __('Next'),
    )
  );

  if( is_array( $pages ) ) {
    $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');

    $pagination = '<nav aria-label="Archive navigation"><ul class="pagination">';

    foreach ( $pages as $page ) {
      $page = str_replace('page-numbers', 'page-link', $page);
      $active = (strpos($page, '>'.$paged.'<') !== false) ? ' active' : '';
      $disabled = (strpos($page, 'dots') !== false) ? ' disabled' : '';
      $pagination .= "<li class='page-item".$active.$disabled."'>$page</li>";
    }

    $pagination .= '</ul></nav>';

    if ( $echo ) {
      echo $pagination;
    } else {
      return $pagination;
    }
  }
}

/**
 * Get language name from Gtranslate language code
 */
function get_gtranslate_language_name($lang) {
    $native_names_map = array(
    'af' => 'Afrikaans',
    'sq' => 'Shqip',
    'am' => 'አማርኛ',
    'ar' => 'العربية',
    'hy' => 'Հայերեն',
    'az' => 'Azərbaycan dili',
    'eu' => 'Euskara',
    'be' => 'Беларуская мова',
    'bn' => 'বাংলা',
    'bs' => 'Bosanski',
    'bg' => 'Български',
    'ca' => 'Català',
    'ceb' => 'Cebuano',
    'ny' => 'Chichewa',
    'zh-CN' => '简体中文',
    'zh-TW' => '繁體中文',
    'co' => 'Corsu',
    'hr' => 'Hrvatski',
    'cs' => 'Čeština‎',
    'da' => 'Dansk',
    'nl' => 'Nederlands',
    'en' => 'English',
    'eo' => 'Esperanto',
    'et' => 'Eesti',
    'tl' => 'Filipino',
    'fi' => 'Suomi',
    'fr' => 'Français',
    'fy' => 'Frysk',
    'gl' => 'Galego',
    'ka' => 'ქართული',
    'de' => 'Deutsch',
    'el' => 'Ελληνικά',
    'gu' => 'ગુજરાતી',
    'ht' => 'Kreyol ayisyen',
    'ha' => 'Harshen Hausa',
    'haw' => 'Ōlelo Hawaiʻi',
    'iw' => 'עִבְרִית',
    'hi' => 'हिन्दी',
    'hmn' => 'Hmong',
    'hu' => 'Magyar',
    'is' => 'Íslenska',
    'ig' => 'Igbo',
    'id' => 'Bahasa Indonesia',
    'ga' => 'Gaelige',
    'it' => 'Italiano',
    'ja' => '日本語',
    'jw' => 'Basa Jawa',
    'kn' => 'ಕನ್ನಡ',
    'kk' => 'Қазақ тілі',
    'km' => 'ភាសាខ្មែរ',
    'ko' => '한국어',
    'ku' => 'كوردی‎',
    'ky' => 'Кыргызча',
    'lo' => 'ພາສາລາວ',
    'la' => 'Latin',
    'lv' => 'Latviešu valoda',
    'lt' => 'Lietuvių kalba',
    'lb' => 'Lëtzebuergesch',
    'mk' => 'Македонски јазик',
    'mg' => 'Malagasy',
    'ms' => 'Bahasa Melayu',
    'ml' => 'മലയാളം',
    'mt' => 'Maltese',
    'mi' => 'Te Reo Māori',
    'mr' => 'मराठी',
    'mn' => 'Монгол',
    'my' => 'ဗမာစာ',
    'ne' => 'नेपाली',
    'no' => 'Norsk bokmål',
    'ps' => 'پښتو',
    'fa' => 'فارسی',
    'pl' => 'Polski',
    'pt' => 'Português',
    'pa' => 'ਪੰਜਾਬੀ',
    'ro' => 'Română',
    'ru' => 'Русский',
    'sm' => 'Samoan',
    'gd' => 'Gàidhlig',
    'sr' => 'Српски језик',
    'st' => 'Sesotho',
    'sn' => 'Shona',
    'sd' => 'سنڌي',
    'si' => 'සිංහල',
    'sk' => 'Slovenčina',
    'sl' => 'Slovenščina',
    'so' => 'Afsoomaali',
    'es' => 'Español',
    'su' => 'Basa Sunda',
    'sw' => 'Kiswahili',
    'sv' => 'Svenska',
    'tg' => 'Тоҷикӣ',
    'ta' => 'தமிழ்',
    'te' => 'తెలుగు',
    'th' => 'ไทย',
    'tr' => 'Türkçe',
    'uk' => 'Українська',
    'ur' => 'اردو',
    'uz' => 'O‘zbekcha',
    'vi' => 'Tiếng Việt',
    'cy' => 'Cymraeg',
    'xh' => 'isiXhosa',
    'yi' => 'יידיש',
    'yo' => 'Yorùbá',
    'zu' => 'Zulu'
    );

    if (isset($native_names_map[$lang])) {
        return $native_names_map[$lang];
    } else {
        return 'English';
    }
}
