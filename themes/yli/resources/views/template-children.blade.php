{{--
  Template Name: Child Page Grid
--}}

@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
    @php
    // WP_Query arguments
    $args = array(
      'post_parent'            => $post->ID,
      'post_type'              => array( 'page' ),
      'posts_per_page'         => '-1',
      'order'                  => 'ASC',
      'orderby'                => 'menu_order',
    );

    // The Query
    $children = new WP_Query( $args );

    // The Loop
    if ( $children->have_posts() ) :
      echo '<div class="row">';
      while ( $children->have_posts() ) :
        $children->the_post();
        @endphp
        @include('partials.content-card')
        @php
      endwhile;
      echo '</div>';
    endif;

    // Restore original Post Data
    wp_reset_postdata();
    @endphp

    {!! get_field('below_subpages') !!}
  @endwhile
@endsection
