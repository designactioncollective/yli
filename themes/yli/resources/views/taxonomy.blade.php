@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
  @include('partials/icons-share')

  @php
  // get the current taxonomy term
  $term = get_queried_object();

  the_field('term_editor', $term);
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
