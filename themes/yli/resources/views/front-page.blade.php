@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{-- Set up section variables --}}
    @php
    $hero     = get_field('hero_section');
    $audience = get_field('audience_links');
    $video    = get_field('video');
    @endphp
    {{-- Hero section --}}
    <div class="home-hero" style="background-image:url('{{ $hero['image']['sizes']['slider'] }}')">
      <div class="container">

      @if ($hero['headline'])
        <h1>{{ $hero['headline'] }}</h1><br>
      @endif

      @if ($hero['subhead'])
        <p>{{ $hero['subhead'] }}</p>
      @endif

      </div>
    </div>

    {{-- Audience links --}}
    <div class="home-audience-links">
      <h2>{{ $audience['section_title'] }}</h2>
      @php
      $i = 1;
      @endphp
      <div class="row">
        @foreach ($audience['audience_link'] as $card)
          <div class="col-md-4">
            <a class="img-card img-card-home" href="{{ $card['link']['url'] }}">
                <div class="thumbnail-bg thumbnail-4x3" style="background-image:url('{{ $card['image']['sizes']['large'] }}')"></div>
              <button class="btn btn-primary btn-block"><img src="@asset('images/audience-'.$i.'.png')" alt="">{{ $card['link']['title'] }}</button>
            </a>
          </div>
          @php
          $i++;
          @endphp
        @endforeach
      </div>
    </div>

    {{-- Full-width video --}}
    @php
      app\the_featured_video('maxresdefault');
    @endphp
  @endwhile

  {{-- Programs Map and Events --}}
  <div class="home-map-events">
    <div class="container">
      <div class="row">
        <div class="col-xl-8">
          <h3 class="home-head home-programs-head">{{ __('Our Programs', 'yli') }}</h3>
          @include('partials.programs-map')
        </div>
        <div class="col-xl-4">
          <h3 class="home-head home-events-head">{{ __('Upcoming Events', 'yli') }}</h3>

          @php
          $args = array(
              'posts_per_page' => 6,
              'start_date' => date( 'Y-m-d H:i:s' ),
              'eventDisplay' => 'list'
          );

          // Run the query
          $events = tribe_get_events( $args );
          @endphp
          @if ($events)
            <ul class="events-widget">
            @foreach ( $events as $post )
              @php setup_postdata( $post ); @endphp
              @include('partials.content-event-mini')
            @endforeach
            </ul>
          @endif
        </div>
      </div>
    </div>
  </div>

  {{-- Featured Stories --}}
  <div class="home-featured-stories">
    <div class="container">
      <h3 class="home-head home-stories-head">{{ __('Featured Stories', 'yli') }}</h3>

      @php
      $args = [
        'posts_per_page' => 3
      ];

      $featured_stories = new WP_Query( $args );
      @endphp
      @if ($featured_stories->have_posts())
        <div class="row">
        @while ($featured_stories->have_posts())
          @php $featured_stories->the_post() @endphp
          @include('partials.content')
        @endwhile
        </div>
      @endif

      <a href="{{ get_page_link(39) }}" class="btn btn-featured-stories">{{ __('View All Stories', 'yli') }}</a>
    </div>
  </div>
@endsection
