@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title( !!}</h1>
  @include('partials/icons-share')

  @php
    the_archive_description();
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @php
    // Get all terms from the policy-year taxonomy--these will be the section headings
    $types = get_terms( [
        'taxonomy' => 'policy-year',
        'hide_empty' => false,
        'order' => 'DESC'
    ] );

    // Set up an empty array -- this will hold all of the reports, sorted into their sections
    $posts_array = array();

    // For each section (as defined by the policy-year taxonomy terms) fill the array with the section title and a sub-array for the posts
    foreach ($types as $type) :
      $posts_array[$type->slug] = [
        'name' => $type->name,
        'posts' => array()
      ];
    endforeach;

  @endphp

  @while (have_posts()) @php the_post() @endphp
    @php
      // Get the policy-year for this particular post
      $type = get_the_terms(null, 'policy-year');
    @endphp

    {{-- If this policy-year has a section (policy-year), capture the content and save it to the right place in the array --}}
    @if (isset($type[0]))
      @php ob_start() @endphp
      @include('partials.content-policy')
      @php $posts_array[$type[0]->slug]['posts'][] = ob_get_clean(); @endphp
    @endif
  @endwhile

  {{-- Output all the posts within their respective sections --}}
  @foreach ($posts_array as $section)
    @if ($section['posts'])
      <h4>{!! $section['name'] !!}</h4>
      <div class="row">
      @foreach ($section['posts'] as $p)
        {!! $p !!}
      @endforeach
      </div>
    @endif
  @endforeach
@endsection
