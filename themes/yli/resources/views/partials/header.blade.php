<header role="banner" id="top">
  <div class="container">
    <div class="header-main">
      <a class="brand" href="{{ home_url('/') }}" rel="home"><img src="@asset('images/logo.png')" alt="{{ get_bloginfo('name', 'display') }}"><span class="sr-only">Every Voice Matters</span></a>
      <aside class="header-meta">
        @include('partials.icons-social')

        <button class="mobile-social-more" aria-hidden="true"></button>

        <div class="dropdown language-dropdown">
          <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if (isset($_SERVER['HTTP_X_GT_LANG']))
              {!! App\get_gtranslate_language_name( $_SERVER['HTTP_X_GT_LANG'] ) !!}
            @else
              {{ 'English' }}
            @endif
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @php echo do_shortcode('[gtranslate]'); @endphp
          </div>
        </div>

        <a href="@php echo get_page_link(163) @endphp" class="btn btn-primary">Donate</a>
      </aside>
    </div>
    <div class="header-nav">
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'slimmenu']) !!}
        @endif
      </nav>
      @php get_search_form() @endphp
    </div>
  </div>
</header>
