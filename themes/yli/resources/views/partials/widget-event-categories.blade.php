<div class="widget" id="widget-event-categories">
  <h3 class="widget-title --sidebar-list">{{ __('Event Categories', 'yli') }}</h3>
  <ul class="sidebar-list">
    @php wp_list_categories( [ 'hide_empty' => 0, 'title_li' => '', 'taxonomy' => 'tribe_events_cat' ] ) @endphp
  </ul>
</div>
