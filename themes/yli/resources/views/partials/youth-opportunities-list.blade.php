@php
  $terms = get_terms( array(
    'taxonomy' => 'opportunity',
    'hide_empty' => false,
    'orderby' => 'term_id',
    'order' => 'ASC'
  ) );
@endphp
<ul class="youth-opportunities-list">
  @foreach ($terms as $term)
    @if (has_term($term->term_id, 'opportunity'))
      @php $icon = '<i class="fas fa-check"></i>'; $class = '--yes'; @endphp
    @else
      @php $icon = '<i class="fas fa-times"></i>'; $class = '--no'; @endphp
    @endif
    <li class="{!! $class !!}">{!! $icon !!}{!! $term->name !!}</li>
  @endforeach
</ul>