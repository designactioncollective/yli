<div class="testimonial col-xs-12">
  {!! get_the_post_thumbnail($post->ID, 'thumbnail', ['class' => 'rounded-circle']) !!}
  <div class="testimonial-body">
    @php the_content(); @endphp
    <p class="attribution">{{ get_the_title() }}</p>
  </div>
</div>
