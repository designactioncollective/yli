@if (get_field('platforms_tax'))
  <h3>Our Strategies</h3>
  @php
    $terms = get_terms( array(
        'taxonomy' => 'strategy',
        'hide_empty' => false,
    ) );
    foreach ($terms as $term) {
      $args = array(
        'posts_per_page' => 4,
        'post_type'      => array('program'),
        'post_status'    => 'publish',
        'order'          => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'platform',
                'field'    => 'id',
                'terms'    => get_field('platforms_tax')
            ),
            array(
                'taxonomy' => 'strategy',
                'field'    => 'id',
                'terms'    => $term->term_taxonomy_id
            ),
          ),
					// 'meta_query'	=> array(
					// 		array(
					// 			'key'	 	=> 'status',
					// 			'value'	  	=> true,
					// 		),
					// 	),
        );

      $posts = get_posts( $args );
      if (sizeOf($posts) != 0) {
        $platforms[$term->term_taxonomy_id] = $posts;
      }
      // else {
      //   $platforms[$term->term_taxonomy_id] = true;
      // }
    }
  @endphp

{{-- <div class="container"> --}}
<div>
  {{-- @php $index = 0; @endphp --}}
  <div class="row">
  @foreach ($terms as $term)

    {{-- @if (($index % 2) === 0)
      @if ($index > 1)
        </div>
      @endif
      <div class="row">
    @endif --}}

    @if ( $platforms[$term->term_taxonomy_id] )
      <div class="col-12 col-md-6">
        @php
          $taxonomy_image = get_field('featured_image', 'strategy_'.$term->term_id);
        @endphp
        <div class="card program-card">
          <a href="/strategy/{{ $term->slug }}" class="img-card">
            @if ($taxonomy_image)
            <div class="thumbnail-bg thumbnail-4x3" style="background-image:url('{{ $taxonomy_image['sizes']['large'] }}')"></div>
            @endif

            <button class="btn btn-block btn-primary">
              {!! $term->name !!}
            </button>
          </a>
          <div class="card-body">
            <strong>{{ __('Featured Programs', 'yli') }}</strong>
            <ul>
              @foreach ( $platforms[$term->term_taxonomy_id] as $post )
                @php
                  // setup_postdata( $post );
                @endphp
                <li><a href="{!! get_permalink($post->ID) !!}">{{ $post->post_title }}</a></li>
              @endforeach
            </ul>
            @php
              wp_reset_postdata();
            @endphp
            <a class="btn btn-block btn-outline-primary" href="/strategy/{{ $term->slug }}">View All {!! $term->name !!} Programs</a>
          </div>
        </div>
      </div>
    @endif

    {{-- @php $index++; @endphp --}}

  @endforeach
  </div>

  @php
    // Timeline query
    $args = array(
    'post_type' => array( 'timeline' ),
    'posts_per_page' => -1,
    'tax_query' => array(
      array(
        'taxonomy' => 'platform',
        'field'    => 'term_id',
        'terms'    => get_field('platforms_tax'),
      ),
    ),
    );
    $timeline = new WP_Query( $args );
    @endphp
    @if ($timeline->have_posts())
    <h3>{{ __('Timeline of Wins in', 'yli') }} {{ get_the_title() }}</h3>
    @while ($timeline->have_posts())
      @php $timeline->the_post() @endphp
      @include('partials/content-timeline')
    @endwhile
  @endif

</div>

{{-- </div> --}}

@endif
