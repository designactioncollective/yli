@php
$application_head = get_field('application_form_heading');
$application_btn  = get_field('application_form_button');
@endphp
@if ($application_head || $application_btn)
  <div class="widget" id="widget-application-form">
    @if ($application_head)
      <h3 class="widget-title">{{ $application_head }}</h3>
    @endif

    @if ($application_btn)
      @php
      $target = ('_blank' === $application_btn['target']) ? ' target="_blank"' : '';
      @endphp
      <a href="{{ $application_btn['url'] }}" class="btn btn-block btn-secondary"{{ $target }}>{{ $application_btn['title'] }}</a>
    @endif
  </div>
@endif
