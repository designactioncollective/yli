{{-- Share icons --}}
@include('partials/icons-share')

{{-- View Shopping Cart --}}
@if (function_exists('is_woocommerce') && is_woocommerce())
  @php
    global $woocommerce;
    $cart_url = $woocommerce->cart->get_cart_url();
  @endphp
  <div class="widget"><a href="{!! $cart_url !!}" class="btn btn-primary btn-block">View Shopping Cart</a></div>
@endif

{{-- Submenu --}}
@if (!is_home() && !is_singular('post') && !is_category() && !is_page([261]))
  @include('partials.widget-submenu')
@endif

{{-- Calls to Action --}}
@if (is_singular('program'))
  @include('partials.widget-calls-to-action')
@endif

{{-- TCS Form --}}
{{-- Main Training & Consulting Services page is ID #91 --}}
{{-- FAQ page is ID #93 --}}
@if (
    (is_page() && (91 == $post->post_parent))
    || is_page(91)
    || is_post_type_archive(['resource', 'testimonial'])
    || is_singular(['resource', 'testimonial']) )

  @if (!is_page(93))
    @include('partials.widget-tcs-form')
  @endif
@endif

{{-- Upcoming Events --}}
{{-- Who We Are is ID #44 --}}
@if (
    is_singular(['program', 'ylumni'])
    || is_post_type_archive(['ylumni'])
    || is_tax('region')
    || is_tax('person-group', [4, 5])
    || is_page(44)
    || (is_page() && (44 == $post->post_parent)) )
  @include('partials.widget-upcoming-events')
@endif

{{-- Application Form --}}
@if (is_singular('program'))
  @include('partials.widget-application-form')
@endif

{{-- Programs by Region & Calls to Action --}}
{{-- Issues is ID #50 --}}
@if (is_page(50) || (50 == $post->post_parent))
  @include('partials.widget-programs')

  @include('partials.widget-calls-to-action')
@endif

{{-- Share Your Story --}}
@if (is_home() || is_singular('post') || is_category())
  @include('partials.widget-share-your-story')
@endif

{{-- Categories --}}
@if (is_home() || is_category())
  @include('partials.widget-categories')
@endif

{{-- Event Categories --}}
@if (is_tax('tribe_events_cat') || is_singular('tribe_events'))
  @include('partials.widget-event-categories')
@endif

{{-- Random Youth Quote --}}
@include('partials.widget-random-quote')
