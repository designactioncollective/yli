<article @php post_class('col-md-4 col-xl-3 person-card') @endphp>
  @if ('' !== $post->post_content)
    <a href="{{ get_permalink() }}">
  @endif
    @if (has_post_thumbnail())
      <div class="thumbnail-bg thumbnail-square" style="background-image:url('@php
      the_post_thumbnail_url('medium')
      @endphp')"></div>
    @endif
  @if ('' !== $post->post_content)
    </a>
  @endif
  <div class="person-card-body">
    <h4 class="entry-title">
      @if ('' !== $post->post_content)
        <a href="{{ get_permalink() }}">
      @endif

      {!! get_the_title() !!}

      @if ('' !== $post->post_content)
        </a>
      @endif
    </h4>
    <ul class="meta">
      @php
      $position   = get_field('position');
      $position_2 = get_field('role');
      $email      = get_field('email');
      $programs   = get_field('programs');

      if ($position)   echo '<li>'.$position.'</li>';
      if ($position_2) echo '<li>'.$position_2.'</li>';
      if ($email)      echo '<li><a href="mailto:'.$email.'">'.$email.'</a></li>';
      if ($programs) :
        echo '<li><ul class="staff-programs">';
        foreach ($programs as $program) :
          echo '<li><a href="'.get_permalink($program->ID).'">'.$program->post_title.'</a></li>';
        endforeach;
        echo '</ul></li>';
      endif;
      @endphp
    </ul>
  </div>
</article>
