@php
$cols = is_front_page() ? 'col-md-4' : 'col-md-6';
@endphp
<article @php post_class($cols) @endphp>
  <div class="story-card">
    @if (has_post_thumbnail())
      <a href="{{ get_permalink() }}" class="image-round-bg" style="background-image: url('{!! get_the_post_thumbnail_url(get_the_ID(), 'large') !!}')"></a>
    @endif
    <div class="card-body">
      <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
      @include('partials/entry-meta')
      <div class="entry-summary">
        @php the_excerpt() @endphp
      </div>
    </div>
  </div>
</article>
