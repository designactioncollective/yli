@php
$upcoming_events_title = __('Upcoming Events', 'yli');

// Basic query
$args = array(
  'posts_per_page' => 4,
  'start_date' => date( 'Y-m-d H:i:s' ),
  'eventDisplay' => 'list',
);

// If region archive, show upcoming events in that region
if (is_tax('region')) {
  $args['tax_query'] = array(
    array(
      'taxonomy' => 'region',
			'field'    => 'term_id',
			'terms'    => get_queried_object_id(),
    ),
  );

  $upcoming_events_title .= ' '.__('in', 'yli').' '.get_queried_object()->name;
}

// If single program, show upcoming events for that program
if (is_singular('program')) {
  $args['meta_query'] = array(
    array(
      'key'     => 'event_program',
      'value'   => get_the_ID(),
      'compare' => '=='
    ),
  );

  $upcoming_events_title = __('Upcoming', 'yli').' '.get_the_title().' '.__('Events', 'yli');
}

// Run the query
$events = tribe_get_events( $args );
@endphp
@if ($events)
  <div class="widget" id="widget-upcoming-events">
    <h3 class="widget-title">{{ $upcoming_events_title }}</h3>
    <ul class="events-widget">
    @foreach ( $events as $post )
      @php setup_postdata( $post ); @endphp
      @include('partials.content-event-mini')
    @endforeach
    </ul>
  </div>
@endif
