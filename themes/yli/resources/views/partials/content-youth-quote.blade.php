<blockquote class="widget">
  @php the_content() @endphp
  <div class="media">
    @if (has_post_thumbnail())
      @php
      the_post_thumbnail('thumbnail')
      @endphp
    @endif
    <div class="media-body">
      {!! get_the_title() !!}
    </div>
  </div>
</blockquote>
