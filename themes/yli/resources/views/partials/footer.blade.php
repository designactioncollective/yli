<footer role="contentinfo">
  <div class="stay-connected">
    <div class="container">
      @php echo do_shortcode('[gravityform id=2 title=true description=false]') @endphp
    </div>
  </div>
  <div class="container">
    <div class="footer-top">
      <a class="brand" href="{{ home_url('/') }}" rel="home"><img src="@asset('images/logo_footer.png')" alt="{{ get_bloginfo('name', 'display') }}"></a>
      <aside class="footer-meta">
        @include('partials.icons-social')

        <div class="footer-button-group">
          <div class="dropdown language-dropdown">
            <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButtonFooter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @if (isset($_SERVER['HTTP_X_GT_LANG']))
                {!! App\get_gtranslate_language_name( $_SERVER['HTTP_X_GT_LANG'] ) !!}
              @else
                {{ 'English' }}
              @endif
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonFooter">
              @php echo do_shortcode('[gtranslate]'); @endphp
            </div>
          </div>

          <a href="@php echo get_page_link(163) @endphp" class="btn btn-primary">Donate</a>
        </div>

        @php get_search_form() @endphp
      </aside>
    </div>

    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav footer-menu', 'depth' => '1', 'container' => '']) !!}

    <a href="{!! get_page_link(46) !!}" class="footer-contact-link">{{ __('Contact Us', 'yli') }}</a>

    @php
    $offices = get_field('offices', 46);
    @endphp
    <ul class="footer-offices">
      @foreach ($offices as $office)
        <li>
          <strong>{{ $office['title'] }}</strong>
          <ul class="office-contact">
            @if ($office['phone'])
              <li>{{ $office['phone'] }}</li>
            @endif
            @if ($office['email'])
              <li><a href="mailto:{{ $office['email'] }}">{{ $office['email'] }}</a></li>
            @endif
          </ul>
        </li>
      @endforeach
    </ul>

    <small class="copyright">&copy; Copyright @php echo date('Y') @endphp, {{ get_bloginfo('name', 'display') }}. {{ __('All Rights Reserved.', 'yli') }} {!! get_field('ein', 46) !!}</small>

    {!! wp_nav_menu(['menu_name' => 'footer-meta', 'menu_class' => 'nav menu-footer-meta', 'container' => '']) !!}

    <a href="#top" class="back-to-top"><i class="fal fa-arrow-circle-up"></i> {{ __('Back to Top', 'yli') }}</a>
  </div>
</footer>
