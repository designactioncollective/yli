<small class="meta">
  <span class="byline author vcard">
    @if (get_field('guest_author'))
      {!! get_field('guest_author') !!}
    @elseif (get_field('author_link'))
      @php $author = get_field('author_link'); @endphp
      <a href="{!! get_permalink($author->ID) !!}">{!! $author->post_title !!}</a>
    @else
      {{ get_the_author() }}
    @endif
  </span>
  &nbsp;|&nbsp;
  <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
  @if (has_category())
  <br>
  @endif
  @php the_category(', ') @endphp
</small>
