<article @php post_class('col-md-6') @endphp>
  <a href="{{ get_permalink() }}" class="img-card">
    @if (has_post_thumbnail())
      <div class="thumbnail-bg thumbnail-4x3" style="background-image:url('@php
      the_post_thumbnail_url('large')
      @endphp')"></div>
    @endif
    <button class="btn btn-block btn-primary">{!! get_the_title() !!}</button>
    @php
    $region = get_the_terms(get_the_ID(), 'region');
    if ($region) :
      $short_name = get_field('short_name', 'region_'.$region[0]->term_id);
      $region_name = $short_name ? $short_name : $region[0]->name;
      echo '<span class="card-region"><i class="fas fa-map-marker-alt"></i> '.$region_name.'</span>';
    endif;
    @endphp
  </a>
</article>
