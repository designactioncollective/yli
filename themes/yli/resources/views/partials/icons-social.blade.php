<nav class="social-icons nav"><?php
  $icons = array(
    array(
      'fa-icon' => 'facebook-f',
      'url'     => 'https://www.facebook.com/youthleadershipinstitute/',
      'title'   => 'Facebook',
    ),
    array(
      'fa-icon' => 'twitter',
      'url'     => 'https://twitter.com/ylinstitute',
      'title'   => 'Twitter',
    ),
    array(
      'fa-icon' => 'linkedin-in',
      'url'     => 'https://www.linkedin.com/company/48032?',
      'title'   => 'LinkedIn',
    ),
    array(
      'fa-icon' => 'instagram',
      'url'     => 'https://instagram.com/ylinstitute',
      'title'   => 'Instagram',
    ),
    array(
      'fa-icon' => 'youtube',
      'url'     => 'https://www.youtube.com/channel/UCKRam2XcoQHvXbMSmTXHGZA',
      'title'   => 'YouTube',
    ),
    /* array(
      'fa-icon' => 'snapchat-ghost',
      'url'     => '#',
      'title'   => 'Snapchat',
    ), */
  );

  foreach ($icons as $icon) :
    $attr = isset($icon['attr']) ? $icon['attr'] : '';
    echo '<a class="nav-item nav-link" href="'.$icon['url'].'" target="_blank" title="'.$icon['title'].'" '.$attr.'><i class="fab fa-'.$icon['fa-icon'].'" aria-hidden="true"></i></a>';
  endforeach;
?></nav>
