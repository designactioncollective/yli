<div class="widget" id="widget-programs">
  <h3 class="widget-title --sidebar-list">{{ __('Programs by Region', 'yli') }}</h3>
  <ul class="sidebar-list">
    @php wp_list_categories( [ 'hide_empty' => 0, 'title_li' => '', 'taxonomy' => 'region' ] ) @endphp
  </ul>
</div>
