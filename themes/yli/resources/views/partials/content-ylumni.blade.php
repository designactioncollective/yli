<article @php post_class('col-md-6') @endphp>
  <a href="{{ get_permalink() }}" class="card">
    @php
    if (get_field('choose_video')) :
      app\the_featured_video();
    else :
      the_post_thumbnail('large', ['class' => 'card-img-top']);
    endif;
    @endphp
    <button class="btn btn-block btn-primary">{!! get_the_title() !!}</button>
  </a>
</article>
