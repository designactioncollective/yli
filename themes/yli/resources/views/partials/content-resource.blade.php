<article @php post_class('resource-card col-md-6 col-xl-12') @endphp>
  @if (has_post_thumbnail())
    <a class="resource-figure" href="{{ get_permalink() }}" style="background-image:url('@php the_post_thumbnail_url('medium') @endphp')"></a>
  @endif
  <div class="card-body">
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    <small class="meta">
    @php
    $author = get_field('author');
    $source = get_field('source');
    $separator = '&emsp;&middot;&emsp;';

    if ($author) echo __('By ', 'yli').'<span class="author vcard">'.$author.'</span>';
    if ($author && $source) echo ', ';
    if ($source) echo 'via <a href="'.$source['url'].'" target="_blank">'.$source['title'].'</a>';
    if ($author || $source) echo $separator;
    the_time('F j, Y');
    the_terms($post->ID, 'resource-type', $separator);
    @endphp
    </small>
    @php the_excerpt() @endphp
  </div>
</article>
