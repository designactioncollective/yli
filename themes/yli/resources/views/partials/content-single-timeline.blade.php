<article @php post_class() @endphp>
  <small class="meta">
    <?php
    the_time('F j, Y');
    the_terms($post->ID, 'region', '&emsp;&middot;&emsp;');
    ?>
  </small>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
