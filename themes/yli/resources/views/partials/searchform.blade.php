<form role="search" method="get" class="search-form" action="{{ esc_url( home_url( '/' ) ) }}">
	<div class="input-group">
		<input type="search" class="form-control" placeholder="{!! esc_attr_x( 'Search &hellip;', 'placeholder' ) !!}" value="{{ get_search_query() }}" name="s" aria-label="Search" aria-describedby="searchFormSubmit" />
		<div class="input-group-append">
			<button type="submit" class="btn btn-outline-secondary" id="searchFormSubmit"><i class="fas fa-search"></i></button>
		</div>
	</div>
</form>
