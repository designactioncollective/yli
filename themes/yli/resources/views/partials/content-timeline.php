<article <?php post_class('timeline-entry') ?>>
  <small class="meta">
    <?php
    the_time('F j, Y');
    $id = get_the_id();
    $terms = the_terms($id, 'region', '&emsp;&middot;&emsp;');

    ?>
  </small>
  <header>
    <h2 class="entry-title">
      <?php if ('' !== $post->post_content) : ?>
        <a href="<?= get_permalink() ?>"><?php the_title() ?></a>
      <?php else :
        the_title();
      endif; ?>
    </h2>
  </header>
  <?php if ('' !== $post->post_content) : ?>
  <div class="entry-summary">
    <?php the_excerpt() ?>
  </div>
  <a href="<?= get_permalink() ?>" class="btn btn-outline-primary"><?= __('Read More', 'yli') ?></a>
  <?php endif; ?>
</article>
