<article @php post_class() @endphp>
  <div class="entry-content">
    @include('partials.widget-youth-opportunities')
    @php
    $name = get_field('staff_contact');
    
    $contact_blurb = (substr( get_the_title(), 0, 3 ) === "The") ? __('Contact', 'yli') : __('Contact the', 'yli');
    @endphp

    @if ($name)
    <aside class="program-contact">
      <h3>{{ $contact_blurb }} {!! get_the_title() !!}</h3>
      <p class="mb-2">{{ __('For more information, contact:', 'yli') }}</p>
        
      @foreach ($name as $n)
        <p class="widget-youth-quote media mb-2 d-flex">
          @php $email = get_field('email', $n->ID); @endphp

          {!! get_the_post_thumbnail($n->ID, 'thumbnail') !!}

          <span style="overflow:hidden;">
            <strong><a href="{!! get_permalink($n->ID) !!}">{!! $n->post_title !!}</a></strong><br>
            <a href="mailto:{{ $email }}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;max-width:100%;display:inline-block;">{{ $email }}</a>
          </span>
          @php $i++; @endphp
        </p>

      @endforeach          
    </aside>
    @endif

    @php the_content() @endphp

  </div>
</article>
