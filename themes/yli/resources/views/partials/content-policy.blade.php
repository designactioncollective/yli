<div class="col-md-6">
  <div class="card program-card">
    <a class="img-card bg-primary" href="{!! get_permalink() !!}">
      @if (has_post_thumbnail())
        {!! get_the_post_thumbnail(null, 'large') !!}
      @endif
      <button class="btn btn-block btn-primary position-static">{!! get_the_title() !!}</button>
    </a>
    <div class="card-body">
      @php the_excerpt() @endphp

      <a class="btn btn-block btn-outline-primary" href="{!! get_permalink() !!}">{!! __('LEARN&nbsp;MORE', 'yli') !!}</a>
    </div>
  </div>
</div>
