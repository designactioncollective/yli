@php
$args = array(
'post_type'		=> 'youth-opportunity',
'posts_per_page'	=> -1,
'meta_query'		=> array(
	array(
		'key' => 'opportunity_program',
		'value' => get_the_ID(),
		'compare' => '='
		)
	)
);

// query
$opportunity = new WP_Query( $args );
@endphp
@if ($opportunity->have_posts())
  @while ($opportunity->have_posts())
    @php $opportunity->the_post() @endphp
    <div class="widget calls-to-action">
      <h3 class="widget-title youth-opportunities-list--title">
        {!! get_field('program_box_title') !!}
        <a class="align-right btn btn-secondary" href="{!! get_permalink() !!}">{!! __('LEARN&nbsp;MORE', 'yli') !!}</a>
      </h3>
      @include('partials.youth-opportunities-list')
    </div>
  @endwhile
{{-- this is repeating code, and I am a bad person for doing it. but I am rushed. --}}
@elseif (is_singular('youth-opportunity'))
  <div class="widget calls-to-action">
    @if (is_post_type_archive('youth-opportunity'))
      <h3 class="widget-title youth-opportunities-list--title">
        {!! get_the_title() !!}
        <a class="align-right btn btn-secondary" href="{!! get_permalink() !!}">{!! __('LEARN&nbsp;MORE', 'yli') !!}</a>
      </h3>
    @endif
    @include('partials.youth-opportunities-list')
  </div>
{{-- post type archive uses a format based on the Program Cards --}}
@elseif (is_post_type_archive('youth-opportunity'))
  <div class="col-md-6">
    <div class="card program-card">
      <a class="img-card bg-primary" href="{!! get_permalink() !!}">
        <button class="btn btn-block btn-primary position-static">{!! get_the_title() !!}</button>
      </a>
      <div class="card-body">
        @include('partials.youth-opportunities-list')

        <a class="btn btn-block btn-outline-primary" href="{!! get_permalink() !!}">{!! __('LEARN&nbsp;MORE', 'yli') !!}</a>
      </div>
    </div>
  </div>
@endif
@php wp_reset_query() @endphp
