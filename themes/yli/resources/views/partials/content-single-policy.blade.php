<article @php post_class() @endphp>
  <div class="entry-content">
    <aside class="program-contact">
      <h3>{{ __('Policy Description', 'yli') }}</h3>
      @php 
      the_excerpt();
      $wycd = get_field('what_you_can_do');
      @endphp
      @if ($wycd)
        <div class="what-you-can-do">
          <h3 class="mt-2">{{ __('What You Can Do', 'yli') }}</h3>
          {!! $wycd !!}
        </div>
      @endif
    </aside>

    @php the_content() @endphp

  </div>
</article>
