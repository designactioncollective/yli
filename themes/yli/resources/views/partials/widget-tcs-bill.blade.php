@php
$tcs_head = get_field('tcs_sidebar_heading', 91);
$tcs_btn  = get_field('tcs_sidebar_button', 91);
@endphp
@if ($tcs_head || $tcs_btn)
  <div class="widget" id="widget-tcs-bill">
    @if ($tcs_head)
      <h3 class="widget-title">{{ $tcs_head }}</h3>
    @endif

    @if ($tcs_btn)
      @php
      $target = ('_blank' === $tcs_btn['target']) ? ' target="_blank"' : '';
      @endphp
      <a href="{{ $tcs_btn['url'] }}" class="btn btn-block btn-primary"{{ $target }}>{{ $tcs_btn['title'] }}</a>
    @endif
  </div>
@endif
