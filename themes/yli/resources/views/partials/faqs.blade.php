@php
$i = 1;
$collapse = ' show';
$ariaExpanded = 'true';
@endphp

<div id="accordion" class="faqs-wrap">

@while (have_rows('faqs'))
  @php the_row(); @endphp

	<div class="faq-card">
		<div class="faq-card-header" id="heading-<?= $i ?>">
      <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?= $i ?>" aria-expanded="<?= $ariaExpanded ?>" aria-controls="collapse-<?= $i ?>">
        @php the_sub_field('question'); @endphp
      </button>
		</div>
		<div id="collapse-<?= $i ?>" class="collapse<?= $collapse ?>" aria-labelledby="heading-<?= $i ?>" data-parent="#accordion">
			<div class="card-body">
				@php the_sub_field('answer'); @endphp
			</div>
		</div>
	</div>
	@php
	// Increase counter, and set following rows to auto-collapsed
	$i++;
	$ariaExpanded = 'false';
	$collapse = '';
	@endphp
@endwhile

</div>
