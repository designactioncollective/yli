<li class="event-mini">
  <div class="event-mini-date">
    <span class="month">{!! tribe_get_start_date( $post->ID, false, 'M' ) !!}</span>
    <span class="day">{!! tribe_get_start_date( $post->ID, false, 'j' ) !!}</span>
  </div>
  <div class="event-mini-body">
    <h4><a href="{{ get_permalink($post) }}">{{ $post->post_title }}</a></h4>
    <small class="meta">
      <time>{!! tribe_get_start_date( $post ) !!}</time>
      @php
      $program = get_field('event_program', $post->ID);
      $region = get_the_terms($post->ID, 'region');

      if ($program) :
        echo '<a href="'.get_permalink($program->ID).'">'.get_the_title($program->ID).'</a>';
      endif;

      if ($program && $region) :
        echo ', ';
      endif;

      if ($region) :
        $short_name = get_field('short_name', 'region_'.$region[0]->term_id);
        $region_name = $short_name ? $short_name : $region[0]->name;

        echo '<a href="'.get_term_link($region[0]->term_id).'">'.$region_name.'</a>';
      endif;
      @endphp
    </small>
  </div>
</li>
