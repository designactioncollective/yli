<article @php post_class() @endphp>
  <ul class="meta">
    @php
    $position   = get_field('position');
    $position_2 = get_field('role');
    $email      = get_field('email');
    $programs   = get_field('programs');

    if ($position)   echo '<li>'.$position.'</li>';
    if ($position_2) echo '<li>'.$position_2.'</li>';
    if ($email)      echo '<li><a href="mailto:'.$email.'">'.$email.'</a></li>';
    if ($programs) :
      echo '<li><ul class="staff-programs">';
      foreach ($programs as $program) :
        echo '<li><a href="'.get_permalink($program->ID).'">'.$program->post_title.'</a></li>';
      endforeach;
      echo '</ul></li>';
    endif;
    @endphp
  </ul>
  <div class="entry-content">
    @php
    the_post_thumbnail('medium', ['class' => 'alignright']);

    the_content();
    @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
