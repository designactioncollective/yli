<?php
$share_title = urlencode(get_bloginfo().wp_title('&raquo;', false));

// Share URL
$parts = parse_url( home_url() );
$share_url = urlencode("{$parts['scheme']}://{$parts['host']}" . add_query_arg( NULL, NULL ));
?>
<ul class="share-icons">
  <li class="share-label">Share </li>
  <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $share_url; ?>&t=" target="_blank" title="Share on Facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
  <li><a href="https://twitter.com/intent/tweet?source=<?php echo $share_url; ?>&text=:%20<?php echo $share_title; ?>&via=" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
  <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $share_url; ?>&title=<?php echo $share_title; ?>&summary=&source=<?php echo $share_url; ?>" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;"><span class="sr-only">Share on LinkedIn</span><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
  <li><a href="http://pinterest.com/pin/create/button/?url=<?php echo $share_url; ?>&description=<?php echo $share_title; ?>" target="_blank" title="Pin it" onclick="window.open('http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(document.URL) + '&description=' +  encodeURIComponent(document.title)); return false;"><i class="fab fa-pinterest-p" aria-hidden="true"></i><span class="sr-only">Pin it</span></a></li>
  <li><a href="mailto:?subject=&body=:<?php echo $share_title; ?>%20:%20<?php echo $share_url; ?>" target="_blank" title="Send email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;"><span class="sr-only">Send email</span><i class="fas fa-envelope" aria-hidden="true"></i></a></li>
</ul>
