@php
$args = array(
	'post_type'              => array( 'youth-quote' ),
	'posts_per_page'         => '1',
	'orderby'                => 'rand',
);

$youthquote = new WP_Query( $args );
@endphp

@if ($youthquote->have_posts())
  <div class="widget widget-youth-quote">
  @while ($youthquote->have_posts())
    @php $youthquote->the_post() @endphp

    @include('partials.content-youth-quote')
  @endwhile
  </div>
@endif

@php wp_reset_postdata() @endphp
