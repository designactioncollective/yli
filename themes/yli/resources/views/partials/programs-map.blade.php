@php
/*
 * Get all programs
 */
$args = array(
	'post_type'              => array( 'program' ),
	'posts_per_page'         => '-1',
);
$programs = new WP_Query( $args );

if ($programs->have_posts()) :
@endphp
<div class="programs-map">
  <img src="{{ get_field('map_image', 137) }}" alt="">
  @php
  $pins = get_field('map_pins', 137);
  $pins_output = '';
  $mobile_output = '<div class="accordion" id="programs-map-mobile">';
  $i = 1;

  foreach ($pins as $pin) :

    // Set up pin coordinates and region
    $coords = explode( ',', $pin['map_pin'] );
    $region = $pin['region'];

    // Run through programs and grab the ones for this region
    $region_programs = array();
    $region_programs_past = array();
    while ($programs->have_posts()) : $programs->the_post();
      if (has_term($region->term_id, 'region')) :
        if (!get_field('status')) :
          $region_programs_past[] = ['title' => get_the_title(), 'permalink' => get_permalink()];
        else :
          $region_programs[] = ['title' => get_the_title(), 'permalink' => get_permalink()];
        endif;
      endif;
    endwhile;
    $programs->rewind_posts(); // rewind for next cycle of foreach

    // Count the programs
    $programs_count = count($region_programs) + count($region_programs_past);

    // Set up popover - current programs
    $programs_list = '<ul>';
    foreach ($region_programs as $p) :
      $programs_list .= '<li><a href=\''.$p['permalink'].'\'>'.$p['title'].'</a></li>';
    endforeach;
    $programs_list .= '</ul>';

    // Popover - past programs
    if ($region_programs_past) :
      $programs_list .= '<h3>'.__('Past Programs', 'yli').'</h3><ul>';
      foreach ($region_programs_past as $p) :
        $programs_list .= '<li><a href=\''.$p['permalink'].'\'>'.$p['title'].'</a></li>';
      endforeach;
      $programs_list .= '</ul>';
    endif;

    // Popover title
    $popover_title = ('statewide' === $region->slug) ? __('Statewide Programs', 'yli') : __('Programs in', 'yli').' '.$region->name;

    $pins_output .= '<a tabindex="0"
             role="button"
             class="map-pin pin-'.$region->slug.'"
             data-toggle="popover"
             data-trigger="focus"
             title="'.$popover_title.'"
             data-content="'.$programs_list.'"
             data-html="true"
             style="left:'.$coords[0].';top:'.$coords[1].';"
             ><i class="fas fa-map-marker-alt"></i><span class="region-label">'.$region->name.'</span> <span class="badge badge-pill badge-secondary">'.$programs_count.'</span></a>';

    $aria_expanded = (1 === $i) ? 'true' : 'false';
    $collapsed = (1 === $i) ? '' : ' collapsed';
    $show = (1 === $i) ? ' show' : '';
    $mobile_output .= '<div class="card">
                         <div id="heading-'.$i.'">
                            <button class="btn btn-block'.$collapsed.'" type="button" data-toggle="collapse" data-target="#collapse-'.$i.'" aria-expanded="'.$aria_expanded.'" aria-controls="collapse-'.$i.'">
                              <span class="mobile-region-label"><i class="fas fa-map-marker-alt"></i>'.$region->name.'</span><span class="badge badge-pill badge-secondary">'.$programs_count.'</span>
                            </button>
                         </div>

                         <div id="collapse-'.$i.'" class="collapse'.$show.'" aria-labelledby="heading-'.$i.'" data-parent="#programs-map-mobile">
                            <div class="card-body">
                              '.$programs_list.'
                            </div>
                          </div>
                        </div>';
    $i++;
  endforeach;

  echo $pins_output;

  // End of Programs query
  endif;
  wp_reset_postdata();

  @endphp
</div>
@php
$mobile_output .= '</div>';
echo $mobile_output;
@endphp
