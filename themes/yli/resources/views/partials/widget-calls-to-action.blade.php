@php
$box_title = get_field('box_title');
$box_content = get_field('box_content');
@endphp
@if ($box_content)
  <div class="widget calls-to-action">
    @if ($box_title)
      <h3 class="widget-title">{!! $box_title !!}</h3>
      {!! $box_content !!}
    @endif
  </div>
@endif
