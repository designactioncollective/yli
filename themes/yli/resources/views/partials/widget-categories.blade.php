<div class="widget" id="widget-categories">
  <h3 class="widget-title --sidebar-list">{{ __('Categories', 'yli') }}</h3>
  <ul class="sidebar-list">
    @php wp_list_categories( [ 'hide_empty' => 0, 'title_li' => '', 'taxonomy' => 'category' ] ) @endphp
  </ul>
</div>
