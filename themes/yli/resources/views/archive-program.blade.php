@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
  @include('partials/icons-share')

  @php
    the_archive_description();
  @endphp

  <h3>{{ __('Our Programs', 'yli') }}</h3>

  <div class="programs-archive-map-wrap">
    @include('partials.programs-map')
  </div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @php
  $regions = get_terms( array(
    'taxonomy' => 'region',
    'hide_empty' => false,
  ) );

  $excluded_ids = array();
  @endphp

  @if ($regions)
    <div class="row">
      @foreach ($regions as $region)
        <div class="col-md-6">
          <div class="card program-card">
            @php
            $img = get_field('featured_image', 'region_'.$region->term_id);
            $short_name = get_field('short_name', 'region_'.$region->term_id);
            $region_name = $short_name ? $short_name : $region->name;
            @endphp
            <a href="{{ get_term_link($region) }}" class="img-card">
              @if ($img)
              <div class="thumbnail-bg thumbnail-4x3" style="background-image:url('{{ $img['sizes']['large'] }}')"></div>
              @endif
              <button class="btn btn-block btn-primary">{{ $region->name }}</button>
            </a>
            <div class="card-body">
              <strong>{{ __('Featured Programs', 'yli') }}</strong>
              <ul>
              @while (have_posts()) @php the_post() @endphp
                @if (has_term($region, 'region') && !in_array(get_the_ID(), $excluded_ids))
                  <li><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></li>
                  @php $excluded_ids[] = get_the_ID(); @endphp
                @endif
              @endwhile
              </ul>
              @php rewind_posts();

              $view_all_label = ('Statewide' === $region_name) ? __('View All Statewide Programs', 'yli') : __('View All Programs in', 'yli').' '.$region_name;
              @endphp
              <a href="{{ get_term_link($region) }}" class="btn btn-block btn-outline-primary">{{ $view_all_label }}</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @endif



@endsection
