{{--
  Template Name: Events
--}}

@extends('layouts.app')

@section('content')
@if(is_singular())
  <h1 class="page-title">{!! get_the_title() !!}</h1>
@endif
@if(is_post_type_archive())
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
@endif
  @include('partials/icons-share')

  <div id="tribe-events-pg-template">
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
  </div> <!-- #tribe-events-pg-template -->
@endsection
