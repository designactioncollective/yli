@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
  @include('partials/icons-share')

  @php
    the_archive_description();
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @php echo do_shortcode('[searchandfilter slug="tools-resources"]'); @endphp

  <div class="row">
    @while (have_posts()) @php the_post() @endphp
      @include('partials.content-'.get_post_type())
    @endwhile
  </div>

  {!! App\bootstrap_pagination( false ) !!}
@endsection
