@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @php
      $id  = get_the_ID();
      $url = wp_get_attachment_url( $id );

      the_excerpt();
    @endphp
    <embed src="{!! $url !!}" width="100%" height="600" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
    <hr>
    @php
      the_content();
    @endphp
  @endwhile
@endsection
