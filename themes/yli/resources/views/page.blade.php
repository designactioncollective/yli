@extends('layouts.app')

@section('content')
  {{-- <h1 class="page-title">{!! get_the_title() !!}</h1> --}}
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    {{-- FAQs --}}
    @if (is_page(93))
      @include('partials.faqs')
    @endif

    {{-- All pages --}}
    @include('partials.content-page')

    {{-- Check and add platform blocks if required --}}
    @include('partials.platform-blocks')

    @if(is_page('tcs'))
    <h3 class="mt-5 mb-4">Read the Latest TCS Stories</h3>
        <div class="row">
          <?php
          $args = (array(
                 'post_type' => 'post',
                 'tag_id' => 70, //training and consulting services
                 'posts_per_page' => 4
               ));
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post(); ?>
                        @include('partials.content-'.get_post_type())
                <?php    }
                } else {
                  //  echo '<h1>no posts found</h1>';
                }
                /* Restore original Post Data */
                wp_reset_postdata();
            ?>
        </div>
      @endif
  @endwhile
@endsection
