@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
    @include('partials.programs-map')
  @endwhile
@endsection
