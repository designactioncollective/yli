{{--
  Template Name: Issue
--}}

@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')

    @php $programs = get_field('programs'); @endphp
    @if ($programs)
      @php
      $args = [
        'post__in'               => $programs,
        'post_type'              => [ 'program' ],
        'posts_per_page'         => '-1',
      ];
      $programs_query = new WP_Query( $args );
      @endphp

      @if ($programs_query->have_posts())
        <h3>{{ __('Programs with a focus on', 'yli') }} {{ get_the_title() }}</h3>
        <div class="row">
        @while ($programs_query->have_posts())
          @php $programs_query->the_post() @endphp
          @include('partials/content-card')
        @endwhile
        @php wp_reset_query(); @endphp
        </div>
      @endif
    @endif

    @php the_field('below_programs') @endphp

    @php $reports = get_field('reports_documents'); @endphp
    @if ($reports)
      <h3>{{ __('Reports & Documents', 'yli') }}</h3>
      <ul class="reports-documents-list">
      @foreach ($reports as $report)
        <li class="mb-2">
          <a class="media" href="{{ $report['upload_file']['url'] }}" target="_blank">
            <i class="fal fa-file-alt"></i>
            <div class="media-body">
              <h4>{{ $report['upload_file']['title'] }}</h4>
              <small>{{ $report['upload_file']['filename'] }}</small>
            </div>
          </a>
        </li>
      @endforeach
      </ul>
    @endif
  @endwhile
@endsection
