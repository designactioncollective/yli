{{--
  Template Name: PDF attachments
  Template Post Type: program, page, post
--}}

@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @if ('program' == get_post_type())
      @include('partials.content-single-program')
    @else
      @include('partials.content-page')
    @endif

    @if (have_rows('attach_pdfs'))
      <div class="row">
      @while (have_rows('attach_pdfs'))
        @php the_row() @endphp
        <div class="col-md-6 col-xl-4 pdf-attachment-wrap">
          @php
            $file        = get_sub_field('pdf');
            $url         = wp_get_attachment_url( $file );
            $view_online = get_attachment_link( $file );
            $thumbnail   = wp_get_attachment_image( $file, 'large', false, ['class' => 'pdf-thumbnail'] );
          @endphp
          {!! $thumbnail !!}
          <div class="btn-group" role="group">
            <a href="{!! $url !!}" target="_blank" class="btn btn-primary">{{ __('Download', 'yli') }}</a>
            <a href="{!! $view_online !!}" class="btn btn-primary">{{ __('View Online', 'yli') }}</a>
          </div>
        </div>
      @endwhile
      </div>
    @endif
  @endwhile
@endsection
