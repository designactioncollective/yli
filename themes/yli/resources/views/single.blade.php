@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  @while(have_posts()) @php the_post() @endphp
    @if (is_singular('post'))
      @include('partials.entry-meta')
    @endif
    @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection
