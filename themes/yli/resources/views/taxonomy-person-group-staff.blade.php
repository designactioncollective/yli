@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
  @include('partials/icons-share')

  @php
  // get the current taxonomy term
  $term = get_queried_object();

  the_field('term_editor', $term);
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @php
  /**
   * Sort terms hierarchically
   * @link https://wordpress.stackexchange.com/questions/14652/how-to-show-a-hierarchical-terms-list
   */
  function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0)
  {
      foreach ($cats as $i => $cat) {
          if ($cat->parent == $parentId) {
              $into[$cat->term_id] = $cat;
              unset($cats[$i]);
          }
      }

      foreach ($into as $topCat) {
          $topCat->children = array();
          sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
      }
  }
  $categories = get_terms('person-group', array('hide_empty' => false));
  $categoryHierarchy = array();
  sort_terms_hierarchicaly($categories, $categoryHierarchy, 4);


  /**
   * Output the headings and staff
   */
  foreach ($categoryHierarchy as $term) :
    echo '<h2>'.$term->name.'</h2>';

    @endphp
    <div class="row">
    @while (have_posts()) @php the_post() @endphp
      @if (has_term($term->term_id, 'person-group'))
        @include('partials.content-'.get_post_type())
      @endif
    @endwhile
    </div>
    @php
    rewind_posts();

    if ($term->children) :
      foreach ($term->children as $child) :
        echo '<h3>'.$child->name.'</h3>';

        @endphp
        <div class="row">
        @while (have_posts()) @php the_post() @endphp
          @if (has_term($child->term_id, 'person-group'))
            @include('partials.content-'.get_post_type())
          @endif
        @endwhile
        </div>
        @php
        rewind_posts();
      endforeach;
    endif;
  endforeach;
  @endphp



  {!! get_the_posts_navigation() !!}
@endsection
