@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_archive_title() !!}</h1>
  @include('partials/icons-share')

  @php
  // get the current taxonomy term
  $term = get_queried_object();

  the_field('term_editor', $term);

  // Past Programs
  $past = array();
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <h3>{{ __('Current Programs', 'yli') }}</h3>
  <div class="row">
  @while (have_posts()) @php the_post() @endphp
    @if (!get_field('status'))
      @php ob_start(); @endphp
    @endif
    @include('partials.content-card')
    @if (!get_field('status'))
      @php $past[] = ob_get_clean(); @endphp
    @endif
  @endwhile
  </div>

  @if (!empty($past))
    <h3>{{ __('Past Programs', 'yli') }}</h3>
    <div class="row">
    @foreach ($past as $p)
      {!! $p !!}
    @endforeach
    </div>
  @endif

  @php
  // Timeline query
  $args = array(
    'post_type' => array( 'timeline' ),
    'posts_per_page' => -1,
    'tax_query' => array(
      array(
        'taxonomy' => 'region',
        'field'    => 'term_id',
        'terms'    => $term->term_id,
      ),
    ),
  );
  $timeline = new WP_Query( $args );
  @endphp
  @if ($timeline->have_posts())
    <h3>{{ __('Timeline of Wins in', 'yli') }} {{ $term->name }}</h3>
    @while ($timeline->have_posts())
      @php $timeline->the_post() @endphp
      @include('partials/content-timeline')
    @endwhile
  @endif
@endsection
