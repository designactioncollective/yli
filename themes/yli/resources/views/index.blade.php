@extends('layouts.app')

@section('content')
  <h1 class="page-title">
    @if(is_posts_page)
    Blog
    @else
    {!! get_the_archive_title() !!}
    @endif
    </h1>
  @include('partials/icons-share')

  @php
    the_archive_description();
  @endphp

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @if (is_home() || is_archive())
  <div class="row">
  @endif

  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile

  @if (is_home() || is_archive())
  </div>
  @endif

  {!! App\bootstrap_pagination( false ) !!}
@endsection
