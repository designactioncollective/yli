@extends('layouts.app')

@section('content')
  <h1 class="page-title">{!! get_the_title() !!}</h1>
  @include('partials/icons-share')

  <div class="mb-3">{!! get_search_form(false) !!}</div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-search')
  @endwhile

  {!! App\bootstrap_pagination( false ) !!}
@endsection
