// import external dependencies
import 'jquery';

// base FontAwesome package
import { library, dom } from '@fortawesome/fontawesome-svg-core';

// Social icons
import { faFacebookF, faTwitter, faYoutube, faFlickr, faPinterestP, faLinkedinIn, faInstagram, faSnapchatGhost } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faSearch, faMapMarkerAlt, faPlay, faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";
import { faHome, faFileAlt, faArrowCircleUp } from "@fortawesome/pro-light-svg-icons";


// add the imported icons to the library
library.add(
  faEnvelope,
  faSearch,
  faMapMarkerAlt,
  faPlay,
  faTimes,
  faCheck,
  faArrowCircleUp,
  faHome,
  faFileAlt,
  faFacebookF,
  faTwitter,
  faYoutube,
  faFlickr,
  faPinterestP,
  faLinkedinIn,
  faInstagram,
  faSnapchatGhost
  );
dom.watch();

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
