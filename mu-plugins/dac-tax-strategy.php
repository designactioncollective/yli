<?php
/*
Plugin Name: Region taxonomy
Description:
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Taxonomy
function strategy_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Strategy', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Strategy', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Strategy', 'dac' ),
		'all_items'                  => __( 'All Strategies', 'dac' ),
		'parent_item'                => __( 'Parent Strategy', 'dac' ),
		'parent_item_colon'          => __( 'Parent Strategy:', 'dac' ),
		'new_item_name'              => __( 'New Strategy Name', 'dac' ),
		'add_new_item'               => __( 'Add New Strategy', 'dac' ),
		'edit_item'                  => __( 'Edit Strategy', 'dac' ),
		'update_item'                => __( 'Update Strategy', 'dac' ),
		'view_item'                  => __( 'View Strategy', 'dac' ),
		'separate_items_with_commas' => __( 'Separate strategies with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove strategies', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Strategies', 'dac' ),
		'search_items'               => __( 'Search Strategies', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No strategies', 'dac' ),
		'items_list'                 => __( 'Strategies list', 'dac' ),
		'items_list_navigation'      => __( 'Strategies list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'							 => true,
	);
	register_taxonomy( 'strategy', array( 'timeline', 'program', 'tribe_events', 'youth-opportunity' ), $args );

}
add_action( 'init', 'strategy_taxonomy', 0 );


// Only show Programs in Strategy term archive. No pagination.
add_filter('pre_get_posts', 'strategy_taxonomy_pre_get_posts');
function strategy_taxonomy_pre_get_posts( $query ) {

    if ( !is_admin() && $query->is_tax( 'strategy' ) && $query->is_main_query() ) {
        $query->set( 'post_type', array( 'program' ) );
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}
