<?php
/*
Plugin Name: Testimonials
Description: Testimonials related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function dac_testimonials_post_type() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Testimonials', 'dac' ),
		'name_admin_bar'        => __( 'Testimonial', 'dac' ),
		'archives'              => __( 'Client &amp; Partner Testimonials', 'dac' ),
		'attributes'            => __( 'Testimonial Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'dac' ),
		'all_items'             => __( 'All Testimonials', 'dac' ),
		'add_new_item'          => __( 'Add New Testimonial', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Testimonial', 'dac' ),
		'edit_item'             => __( 'Edit Testimonial', 'dac' ),
		'update_item'           => __( 'Update Testimonial', 'dac' ),
		'view_item'             => __( 'View Testimonial', 'dac' ),
		'view_items'            => __( 'View Testimonials', 'dac' ),
		'search_items'          => __( 'Search Testimonial', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'dac' ),
		'items_list'            => __( 'Testimonials list', 'dac' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Testimonials list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'testimonial',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'dac' ),
		'description'           => __( 'Testimonials', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 29,
		'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'testimonials',
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'dac_testimonials_post_type', 0 );

// Change "Enter Title Here" to "Testimonial Author"
function dac_change_title_text_testimonial( $title ){
     $screen = get_current_screen();
  
     if  ( 'testimonial' == $screen->post_type ) {
          $title = 'Testimonial Author';
     }
  
     return $title;
}
add_filter( 'enter_title_here', 'dac_change_title_text_testimonial' );