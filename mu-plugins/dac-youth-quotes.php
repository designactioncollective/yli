<?php
/*
Plugin Name: Youth Quotes
Description: Youth Quotes related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function dac_youth_quotes_post_type() {

	$labels = array(
		'name'                  => _x( 'Youth Quotes', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Youth Quote', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Youth Quotes', 'dac' ),
		'name_admin_bar'        => __( 'Youth Quote', 'dac' ),
		'archives'              => __( 'Youth Quote Archives', 'dac' ),
		'attributes'            => __( 'Youth Quote Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Youth Quote:', 'dac' ),
		'all_items'             => __( 'All Youth Quotes', 'dac' ),
		'add_new_item'          => __( 'Add New Youth Quote', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Youth Quote', 'dac' ),
		'edit_item'             => __( 'Edit Youth Quote', 'dac' ),
		'update_item'           => __( 'Update Youth Quote', 'dac' ),
		'view_item'             => __( 'View Youth Quote', 'dac' ),
		'view_items'            => __( 'View Youth Quotes', 'dac' ),
		'search_items'          => __( 'Search Youth Quote', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'insert_into_item'      => __( 'Insert into Youth Quote', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Youth Quote', 'dac' ),
		'items_list'            => __( 'Youth Quotes list', 'dac' ),
		'items_list_navigation' => __( 'Youth Quotes list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Youth Quotes list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'youth-quote',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Youth Quotes', 'dac' ),
		'description'           => __( 'Youth Quotes', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 30,
		'menu_icon'             => 'dashicons-editor-quote',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'youth-quote', $args );

}
add_action( 'init', 'dac_youth_quotes_post_type', 0 );

// Change "Enter Title Here" to "Youth Quote Author"
function dac_change_title_text_youth_quote( $title ){
     $screen = get_current_screen();
  
     if  ( 'youth-quote' == $screen->post_type ) {
          $title = 'Youth Quote Author';
     }
  
     return $title;
}
add_filter( 'enter_title_here', 'dac_change_title_text_youth_quote' );


// Add shortcode columns

add_filter('manage_edit-youth-quote_columns', 'my_columns');
function my_columns($columns) {
	$columns['shortcode'] = 'Shortcode';
	return $columns;
}

add_action('manage_posts_custom_column', 'my_show_columns');
function my_show_columns($name) {
	global $post;
	switch ($name) {
		case 'shortcode':
		$shortcode = '<input type="text" value="[youthquote id=&quot;'.$post->ID.'&quot;]">';
		echo $shortcode;
	}
}

// Add metabox with shortcode to slideshow posts
function add_shortcode_metabox() {
    add_meta_box('youth_quote_metabox_shortcode', 'Shortcode', 'youth_quote_metabox_callback', 'youth-quote', 'side', 'default');
}
add_action( 'add_meta_boxes', 'add_shortcode_metabox' );

function youth_quote_metabox_callback() {
	global $post;
	echo '<p>Copy and paste the following shortcode into the post or page where you would like this youth quote to appear:</p><input type="text" value="[youthquote id=&quot;'.$post->ID.'&quot;]">';
}

// Add Shortcode
function youth_quote_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'id' => '',
		), $atts )
	);
	
	$quote = get_post($id);
	
	$return = '<div class="media">'.get_the_post_thumbnail($quote, 'thumbnail', ['class' => 'rounded-circle mr-3 align-self-center']).'<div class="media-body align-self-center">&ldquo;'.$quote->post_content.'&rdquo;<br><strong>'.$quote->post_title.'</strong></div></div>';
	
	return $return;
}
add_shortcode( 'youthquote', 'youth_quote_shortcode' );