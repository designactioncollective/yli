<?php
/*
Plugin Name: Region taxonomy
Description:
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Taxonomy
function region_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Region', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Region', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Region', 'dac' ),
		'all_items'                  => __( 'All Regions', 'dac' ),
		'parent_item'                => __( 'Parent Region', 'dac' ),
		'parent_item_colon'          => __( 'Parent Region:', 'dac' ),
		'new_item_name'              => __( 'New Region Name', 'dac' ),
		'add_new_item'               => __( 'Add New Region', 'dac' ),
		'edit_item'                  => __( 'Edit Region', 'dac' ),
		'update_item'                => __( 'Update Region', 'dac' ),
		'view_item'                  => __( 'View Region', 'dac' ),
		'separate_items_with_commas' => __( 'Separate regions with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove regions', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Regions', 'dac' ),
		'search_items'               => __( 'Search Regions', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No regions', 'dac' ),
		'items_list'                 => __( 'Regions list', 'dac' ),
		'items_list_navigation'      => __( 'Regions list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'							 => true,
	);
	register_taxonomy( 'region', array( 'timeline', 'program', 'tribe_events', 'youth-opportunity' ), $args );

}
add_action( 'init', 'region_taxonomy', 0 );


// Only show Programs in Region term archive. No pagination.
add_filter('pre_get_posts', 'region_taxonomy_pre_get_posts');
function region_taxonomy_pre_get_posts( $query ) {

    if ( !is_admin() && $query->is_tax( 'region' ) && $query->is_main_query() ) {
        $query->set( 'post_type', array( 'program' ) );
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}
