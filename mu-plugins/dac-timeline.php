<?php
/*
Plugin Name: Timeline
Description: Timeline related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function timeline_post_type() {

	$labels = array(
		'name'                  => _x( 'Timeline Entries', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Timeline', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Timeline', 'dac' ),
		'name_admin_bar'        => __( 'Timeline', 'dac' ),
		'archives'              => __( 'Entry Archives', 'dac' ),
		'attributes'            => __( 'Entry Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Entry:', 'dac' ),
		'all_items'             => __( 'All Entries', 'dac' ),
		'add_new_item'          => __( 'Add New Entry', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Entry', 'dac' ),
		'edit_item'             => __( 'Edit Entry', 'dac' ),
		'update_item'           => __( 'Update Entry', 'dac' ),
		'view_item'             => __( 'View Entry', 'dac' ),
		'view_items'            => __( 'View Entries', 'dac' ),
		'search_items'          => __( 'Search Entry', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into item', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
		'items_list'            => __( 'Entries list', 'dac' ),
		'items_list_navigation' => __( 'Entries list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter entries list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Timeline', 'dac' ),
		'description'           => __( 'Timeline', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-editor-ul',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'timeline', $args );

}
add_action( 'init', 'timeline_post_type', 0 );

// Register Custom Taxonomy
function timeline_entry_type_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Entry Type', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Entry Type', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Entry Type', 'dac' ),
		'all_items'                  => __( 'All Entry Types', 'dac' ),
		'parent_item'                => __( 'Parent Entry Type', 'dac' ),
		'parent_item_colon'          => __( 'Parent Entry Type:', 'dac' ),
		'new_item_name'              => __( 'New Entry Type Name', 'dac' ),
		'add_new_item'               => __( 'Add New Entry Type', 'dac' ),
		'edit_item'                  => __( 'Edit Entry Type', 'dac' ),
		'update_item'                => __( 'Update Entry Type', 'dac' ),
		'view_item'                  => __( 'View Entry Type', 'dac' ),
		'separate_items_with_commas' => __( 'Separate entries with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove entries', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Entry Types', 'dac' ),
		'search_items'               => __( 'Search Entry Types', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No entries', 'dac' ),
		'items_list'                 => __( 'Entry Types list', 'dac' ),
		'items_list_navigation'      => __( 'Entry Types list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'entry-type', array( 'timeline' ), $args );

}
add_action( 'init', 'timeline_entry_type_taxonomy', 0 );