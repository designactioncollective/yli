<?php
/*
Plugin Name: Policies
Description: Policies related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function dac_policy_post_type() {

	$labels = array(
		'name'                  => _x( 'Policies We Support', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Policy', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Policies', 'dac' ),
		'name_admin_bar'        => __( 'Policy', 'dac' ),
		'archives'              => __( 'Policies', 'dac' ),
		'attributes'            => __( 'Policy Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Policy:', 'dac' ),
		'all_items'             => __( 'All Policies', 'dac' ),
		'add_new_item'          => __( 'Add New Policy', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Policy', 'dac' ),
		'edit_item'             => __( 'Edit Policy', 'dac' ),
		'update_item'           => __( 'Update Policy', 'dac' ),
		'view_item'             => __( 'View Policy', 'dac' ),
		'view_items'            => __( 'View Policies', 'dac' ),
		'search_items'          => __( 'Search Policy', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'insert_into_item'      => __( 'Insert into Policy', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Policy', 'dac' ),
		'items_list'            => __( 'Policies list', 'dac' ),
		'items_list_navigation' => __( 'Policies list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Policies list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'policy',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Policy', 'dac' ),
		'description'           => __( 'Policies', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 29,
		'menu_icon'             => 'dashicons-yes-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'policies',
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'policy', $args );

}
add_action( 'init', 'dac_policy_post_type', 0 );

// Register Custom Taxonomy
function policies_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Policy Year', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Policy Year', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Policy Year', 'dac' ),
		'all_items'                  => __( 'All Policy Years', 'dac' ),
		'parent_item'                => __( 'Parent Policy Year', 'dac' ),
		'parent_item_colon'          => __( 'Parent Policy Year:', 'dac' ),
		'new_item_name'              => __( 'New Policy Year Name', 'dac' ),
		'add_new_item'               => __( 'Add New Policy Year', 'dac' ),
		'edit_item'                  => __( 'Edit Policy Year', 'dac' ),
		'update_item'                => __( 'Update Policy Year', 'dac' ),
		'view_item'                  => __( 'View Policy Year', 'dac' ),
		'separate_items_with_commas' => __( 'Separate Policy Years with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove Policy Years', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Policy Years', 'dac' ),
		'search_items'               => __( 'Search Policy Years', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No Policy Years', 'dac' ),
		'items_list'                 => __( 'Policy Years list', 'dac' ),
		'items_list_navigation'      => __( 'Policy Years list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'policy-year', array( 'policy' ), $args );

}
add_action( 'init', 'policies_taxonomy', 0 );

// No pagination.
add_filter('pre_get_posts', 'policies_pre_get_posts');
function policies_pre_get_posts( $query ) {

    if ( !is_admin() && $query->is_post_type_archive( 'policy' ) && $query->is_main_query() ) {
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}