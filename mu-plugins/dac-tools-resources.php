<?php
/*
Plugin Name: Tools and Resources
Description: Tools and Resources related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function tools_resources_post_type() {

	$labels = array(
		'name'                  => _x( 'Tools &amp; Resources', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Tools &amp; Resources', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Tools &amp; Resources', 'dac' ),
		'name_admin_bar'        => __( 'Tools &amp; Resources', 'dac' ),
		'archives'              => __( 'Tools &amp; Resources', 'dac' ),
		'attributes'            => __( 'Tool or Resource Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Tool or Resource:', 'dac' ),
		'all_items'             => __( 'All Tools &amp; Resources', 'dac' ),
		'add_new_item'          => __( 'Add New Tool or Resource', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Tool or Resource', 'dac' ),
		'edit_item'             => __( 'Edit Tool or Resource', 'dac' ),
		'update_item'           => __( 'Update Tool or Resource', 'dac' ),
		'view_item'             => __( 'View Tool or Resource', 'dac' ),
		'view_items'            => __( 'View Tools &amp; Resources', 'dac' ),
		'search_items'          => __( 'Search Tool or Resource', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into tool or resource', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this tool or resource', 'dac' ),
		'items_list'            => __( 'Tools &amp; Resources list', 'dac' ),
		'items_list_navigation' => __( 'Tools &amp; Resources list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter tools and resources list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Tools &amp; Resources', 'dac' ),
		'description'           => __( 'Tools and resources', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'tools-resources',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'resource', $args );

}
add_action( 'init', 'tools_resources_post_type', 0 );


// Register Custom Taxonomy
function resource_type_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Resource Type', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Resource Type', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Resource Type', 'dac' ),
		'all_items'                  => __( 'All Resource Types', 'dac' ),
		'parent_item'                => __( 'Parent Resource Type', 'dac' ),
		'parent_item_colon'          => __( 'Parent Resource Type:', 'dac' ),
		'new_item_name'              => __( 'New Resource Type Name', 'dac' ),
		'add_new_item'               => __( 'Add New Resource Type', 'dac' ),
		'edit_item'                  => __( 'Edit Resource Type', 'dac' ),
		'update_item'                => __( 'Update Resource Type', 'dac' ),
		'view_item'                  => __( 'View Resource Type', 'dac' ),
		'separate_items_with_commas' => __( 'Separate types with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove types', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Resource Types', 'dac' ),
		'search_items'               => __( 'Search Resource Types', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No types', 'dac' ),
		'items_list'                 => __( 'Resource Types list', 'dac' ),
		'items_list_navigation'      => __( 'Resource Types list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'resource-type', array( 'resource' ), $args );

}
add_action( 'init', 'resource_type_taxonomy', 0 );
