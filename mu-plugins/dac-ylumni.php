<?php
/*
Plugin Name: YLumnI
Description: YLumnI related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function ylumni_post_type() {

	$labels = array(
		'name'                  => _x( 'YLumnI', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'YLumnI', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'YLumnI', 'dac' ),
		'name_admin_bar'        => __( 'YLumnI', 'dac' ),
		'archives'              => __( 'YLumnI', 'dac' ),
		'attributes'            => __( 'YLumnI Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent YLumnI:', 'dac' ),
		'all_items'             => __( 'All YLumnI', 'dac' ),
		'add_new_item'          => __( 'Add New YLumnI', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New YLumnI', 'dac' ),
		'edit_item'             => __( 'Edit YLumnI', 'dac' ),
		'update_item'           => __( 'Update YLumnI', 'dac' ),
		'view_item'             => __( 'View YLumnI', 'dac' ),
		'view_items'            => __( 'View YLumnI', 'dac' ),
		'search_items'          => __( 'Search YLumnI', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into YLumnI', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this YLumnI', 'dac' ),
		'items_list'            => __( 'YLumnI list', 'dac' ),
		'items_list_navigation' => __( 'YLumnI list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter entries list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'YLumnI', 'dac' ),
		'description'           => __( 'YLumnI', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-id-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'ylumni', $args );

}
add_action( 'init', 'ylumni_post_type', 0 );

