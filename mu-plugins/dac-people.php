<?php
/*
Plugin Name: People
Description: People related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Taxonomies
function dac_people_taxonomies() {
	
	// Type
	$labels = array(
		'name'                       => _x( 'Groups', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Group', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Group', 'dac' ),
		'all_items'                  => __( 'All Groups', 'dac' ),
		'parent_item'                => __( 'Parent Group', 'dac' ),
		'parent_item_colon'          => __( 'Parent Group:', 'dac' ),
		'new_item_name'              => __( 'New Group Name', 'dac' ),
		'add_new_item'               => __( 'Add New Group', 'dac' ),
		'edit_item'                  => __( 'Edit Group', 'dac' ),
		'update_item'                => __( 'Update Group', 'dac' ),
		'view_item'                  => __( 'View Group', 'dac' ),
		'separate_items_with_commas' => __( 'Separate groups with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove groups', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Groups', 'dac' ),
		'search_items'               => __( 'Search Groups', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No Groups', 'dac' ),
		'items_list'                 => __( 'Groups list', 'dac' ),
		'items_list_navigation'      => __( 'Groups list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'										 => array('slug' => 'people', 'with_front' => false)
	);
	register_taxonomy( 'person-group', array( 'people' ), $args );
	
}
add_action( 'init', 'dac_people_taxonomies', 0 );

// Register Custom Post Type
function people_post_type() {

	$labels = array(
		'name'                  => _x( 'People', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Person', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'People', 'dac' ),
		'name_admin_bar'        => __( 'Person', 'dac' ),
		'archives'              => __( 'Person Archives', 'dac' ),
		'attributes'            => __( 'Person Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Person:', 'dac' ),
		'all_items'             => __( 'All People', 'dac' ),
		'add_new_item'          => __( 'Add New Person', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Person', 'dac' ),
		'edit_item'             => __( 'Edit Person', 'dac' ),
		'update_item'           => __( 'Update Person', 'dac' ),
		'view_item'             => __( 'View Person', 'dac' ),
		'view_items'            => __( 'View People', 'dac' ),
		'search_items'          => __( 'Search Person', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Headshot', 'dac' ),
		'set_featured_image'    => __( 'Set headshot', 'dac' ),
		'remove_featured_image' => __( 'Remove headshot', 'dac' ),
		'use_featured_image'    => __( 'Use as headshot', 'dac' ),
		'insert_into_item'      => __( 'Insert into item', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
		'items_list'            => __( 'People list', 'dac' ),
		'items_list_navigation' => __( 'People list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter People list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'person',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Person', 'dac' ),
		'description'           => __( 'People', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-id-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'people',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'rewrite'								=> $rewrite,
		'show_in_rest'          => true,
	);
	register_post_type( 'people', $args );

}
add_action( 'init', 'people_post_type', 0 );



// Auto add and update Title field for Staff and Board
// via https://support.advancedcustomfields.com/forums/topic/replacing-custom-post-type-post-title-with-an-acf/
function person_update_title( $value, $post_id, $field ) {
	
	$new_title = get_field( 'first_name', $post_id) . ' ' . $value;
	$new_slug = sanitize_title( $new_title );
	
	// update post
	$person_postdata = array(
		'ID'          => $post_id,
		'post_title'  => $new_title,
		'post_name'   => $new_slug,
	);	
	
	if ( ! wp_is_post_revision( $post_id ) ){
	
		// unhook this function so it doesn't loop infinitely
		remove_action('save_post', 'person_update_title');
	
		// update the post, which calls save_post again
		wp_update_post( $person_postdata );

		// re-hook this function
		add_action('save_post', 'person_update_title');
	}	
	
	return $value;
}

add_filter('acf/update_value/name=last_name', 'person_update_title', 10, 3);


// Order by last name
function person_archive_pre_get_posts( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_post_type_archive( 'person' ) || is_tax('person-group') ) {
        $query->set( 'posts_per_page', -1 );
        $query->set( 'orderby', 'meta_value' );
				$query->set( 'meta_key', 'last_name' );
				$query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts', 'person_archive_pre_get_posts', 1 );


/*
 * Add opening tag before start of loop
 */
add_action( 'loop_start', 'people_loop_start' );
function people_loop_start( $query ){
	if ( (is_post_type_archive('people') || (is_tax('person-group') && !is_tax('person-group', 'staff'))) && $query->is_main_query() ) {
		?>
		<div class="row">
		<?php
	}
}

/*
 * Add closing tag at end of loop
 */
add_action( 'loop_end', 'people_loop_end' );
function people_loop_end( $query ){
	if ( (is_post_type_archive('people') || (is_tax('person-group') && !is_tax('person-group', 'staff'))) && $query->is_main_query() ) {
		?>
		</div>
		<?php
	}
}