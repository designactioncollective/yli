<?php
/*
Plugin Name: Programs
Description: Programs related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function programs_post_type() {

	$labels = array(
		'name'                  => _x( 'Programs', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Program', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Programs', 'dac' ),
		'name_admin_bar'        => __( 'Program', 'dac' ),
		'archives'              => __( 'Programs', 'dac' ),
		'attributes'            => __( 'Program Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Program:', 'dac' ),
		'all_items'             => __( 'All Programs', 'dac' ),
		'add_new_item'          => __( 'Add New Program', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Program', 'dac' ),
		'edit_item'             => __( 'Edit Program', 'dac' ),
		'update_item'           => __( 'Update Program', 'dac' ),
		'view_item'             => __( 'View Program', 'dac' ),
		'view_items'            => __( 'View Programs', 'dac' ),
		'search_items'          => __( 'Search Program', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into program', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this program', 'dac' ),
		'items_list'            => __( 'Programs list', 'dac' ),
		'items_list_navigation' => __( 'Programs list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter programs list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Program', 'dac' ),
		'description'           => __( 'Programs', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-clipboard',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'programs',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'program', $args );

}
add_action( 'init', 'programs_post_type', 0 );


// Only show Featured programs
add_filter('pre_get_posts', 'programs_post_type_pre_get_posts');
function programs_post_type_pre_get_posts( $query ) {
		if (is_admin()) return;
		
    if ( $query->is_post_type_archive( 'program' ) && $query->is_main_query() ) {
        $meta_query = array( array(
            'key'		=> 'feature_on_programs_landing',
            'value'		=> 1,
            'compare' => '='
        ) );
        $query->set( 'meta_query', $meta_query );
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}