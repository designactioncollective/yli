<?php
/*
Plugin Name: TCS Map
Description: TCS Map related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function tcs_map_post_type() {

	$labels = array(
		'name'                  => _x( 'TCS Map', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'TCS Map', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'TCS Map', 'dac' ),
		'name_admin_bar'        => __( 'TCS Map', 'dac' ),
		'archives'              => __( 'Location Archives', 'dac' ),
		'attributes'            => __( 'Location Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Location:', 'dac' ),
		'all_items'             => __( 'All Locations', 'dac' ),
		'add_new_item'          => __( 'Add New Location', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Location', 'dac' ),
		'edit_item'             => __( 'Edit Location', 'dac' ),
		'update_item'           => __( 'Update Location', 'dac' ),
		'view_item'             => __( 'View Location', 'dac' ),
		'view_items'            => __( 'View Locations', 'dac' ),
		'search_items'          => __( 'Search Location', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into location', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this location', 'dac' ),
		'items_list'            => __( 'Locations list', 'dac' ),
		'items_list_navigation' => __( 'Locations list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter locations list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'TCS Map', 'dac' ),
		'description'           => __( 'TCS Map', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'tcs', $args );

}
add_action( 'init', 'tcs_map_post_type', 0 );

/**
 * Set Google API Key
 */
function dac_acf_google_api_key() {
	//$api['key'] = 'AIzaSyC_GLV1TLqW3pKGI-EvwQ_j_K2HVUv2nsg';
	//$api['key'] = 'AIzaSyAjkQp4pAMCWqI-zS_iLXtQk3UzfmMnM9k';
	$api['libraries'] = 'places';
	$api['key'] = 'AIzaSyCe5auCXKadGQGAZoAHNKietpao6lX7RIA';
	
	return $api;
}
add_filter('acf/fields/google_map/api', 'dac_acf_google_api_key');

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCe5auCXKadGQGAZoAHNKietpao6lX7RIA');
}

add_action('acf/init', 'my_acf_init');

/**
 * Output shortcode
 *
 * @link https://stackoverflow.com/questions/49886691/acf-google-map-marker-clustering
 */
function tcs_past_trainings_map_shortcode() {
	ob_start();
	?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe5auCXKadGQGAZoAHNKietpao6lX7RIA"></script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
	<script type="text/javascript">
	(function($) {

	/*
	*  new_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type    function
	*  @date    8/11/2013
	*  @since   4.3.0
	*
	*  @param   $el (jQuery element)
	*  @return  n/a
	*/

	function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
			zoom        : 16,
			center      : new google.maps.LatLng(46.8, 1.9),
			mapTypeId   : google.maps.MapTypeId.ROADMAP
	};


	// create map               
	var map = new google.maps.Map( $el[0], args);


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

			add_marker( $(this), map );

	});


	// center map
	center_map( map );

	// add marker cluster
	markerCluster( map.markers, map )

	// return
	return map;

	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type    function
	*  @date    8/11/2013
	*  @since   4.3.0
	*
	*  @param   $marker (jQuery element)
	*  @param   map (Google Map object)
	*  @return  n/a
	*/

	function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
			position    : latlng,
			map         : map
	});



	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{

			// create info window
			var infowindow = new google.maps.InfoWindow({
					content     : $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

					if (typeof( window.infoopened ) != 'undefined') infoopened.close();
					infowindow.open(map,marker);
					infoopened = infowindow;

			});
	}


	}

	function markerCluster( markers, map ) {
			var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
			//console.log( markers );
	}
	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type    function
	*  @date    8/11/2013
	*  @since   4.3.0
	*  @param   map (Google Map object)
	*  @return  n/a
	*/

	function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
			// set center of map
			map.setCenter( bounds.getCenter() );
			map.setZoom( 16 );
	}
	else
	{
			// fit to bounds
			map.fitBounds( bounds );
	}

	}


	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type    function
	*  @date    8/11/2013
	*  @since   5.0.0
	*
	*  @param   n/a
	*  @return  n/a
	*/
	// global var
	var map = null;

	$(document).ready(function(){

	$('.acf-map').each(function(){

			// create map
			map = new_map( $(this) );

	});

	});

	})(jQuery);
	</script>
	<?php
	$scripts = ob_get_clean();

	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'tcs' ),
		'posts_per_page'         => '-1',
	);

	// The Query
	$markers = new WP_Query( $args );

	// The Loop
	if ( $markers->have_posts() ) {
	
		$return = '<div class="acf-map">';
	
		while ( $markers->have_posts() ) {
			$markers->the_post();
			
			$pin = get_field('tcs_map_pin');
			
			if ( !empty($pin) ) {
				$return .= '<div class="marker" data-lat="'.$pin['lat'].'" data-lng="'.$pin['lng'].'">'.get_the_title().'</div>';
			}
		}
		
		$return .= '</div>';
		
		$return .= $scripts;
		
	} else {
		$return = '';
	}

	// Restore original Post Data
	wp_reset_postdata();

	return $return;
}
add_shortcode( 'tcs-past-trainings-map', 'tcs_past_trainings_map_shortcode' );