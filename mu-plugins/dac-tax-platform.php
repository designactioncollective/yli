<?php
/*
Plugin Name: Region taxonomy
Description:
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Taxonomy
function Platform_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Platform', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Platform', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Platform', 'dac' ),
		'all_items'                  => __( 'All Platforms', 'dac' ),
		'parent_item'                => __( 'Parent Platform', 'dac' ),
		'parent_item_colon'          => __( 'Parent Platform:', 'dac' ),
		'new_item_name'              => __( 'New Platform Name', 'dac' ),
		'add_new_item'               => __( 'Add New Platform', 'dac' ),
		'edit_item'                  => __( 'Edit Platform', 'dac' ),
		'update_item'                => __( 'Update Platform', 'dac' ),
		'view_item'                  => __( 'View Platform', 'dac' ),
		'separate_items_with_commas' => __( 'Separate Platforms with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove Platforms', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Platforms', 'dac' ),
		'search_items'               => __( 'Search Platforms', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No Platforms', 'dac' ),
		'items_list'                 => __( 'Platforms list', 'dac' ),
		'items_list_navigation'      => __( 'Platforms list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'							 => true,
	);
	register_taxonomy( 'platform', array( 'timeline', 'program', 'tribe_events', 'youth-opportunity' ), $args );

}
add_action( 'init', 'Platform_taxonomy', 0 );


// Only show Programs in Platform term archive. No pagination.
add_filter('pre_get_posts', 'Platform_taxonomy_pre_get_posts');
function Platform_taxonomy_pre_get_posts( $query ) {

    if ( !is_admin() && $query->is_tax( 'Platform' ) && $query->is_main_query() ) {
        $query->set( 'post_type', array( 'program' ) );
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}
