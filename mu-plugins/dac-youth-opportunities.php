<?php
/*
Plugin Name: Youth Opportunities
Description: Youth Opportunities related functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( '' );

// Register Custom Post Type
function dac_youth_opportunities_post_type() {

	$labels = array(
		'name'                  => _x( 'Youth Opportunities', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Youth Opportunity', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Youth Opportunities', 'dac' ),
		'name_admin_bar'        => __( 'Youth Opportunity', 'dac' ),
		'archives'              => __( 'Youth Opportunities', 'dac' ),
		'attributes'            => __( 'Youth Opportunity Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Youth Opportunity:', 'dac' ),
		'all_items'             => __( 'All Youth Opportunities', 'dac' ),
		'add_new_item'          => __( 'Add New Youth Opportunity', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Youth Opportunity', 'dac' ),
		'edit_item'             => __( 'Edit Youth Opportunity', 'dac' ),
		'update_item'           => __( 'Update Youth Opportunity', 'dac' ),
		'view_item'             => __( 'View Youth Opportunity', 'dac' ),
		'view_items'            => __( 'View Youth Opportunities', 'dac' ),
		'search_items'          => __( 'Search Youth Opportunity', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'insert_into_item'      => __( 'Insert into Youth Opportunity', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Youth Opportunity', 'dac' ),
		'items_list'            => __( 'Youth Opportunities list', 'dac' ),
		'items_list_navigation' => __( 'Youth Opportunities list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Youth Opportunities list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'youth-opportunity',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Youth Opportunity', 'dac' ),
		'description'           => __( 'Youth Opportunities', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 29,
		'menu_icon'             => 'dashicons-forms',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'youth-opportunities',
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'youth-opportunity', $args );

}
add_action( 'init', 'dac_youth_opportunities_post_type', 0 );

// Register Custom Taxonomy
function youth_opportunity_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Opportunity', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Opportunity', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Opportunity', 'dac' ),
		'all_items'                  => __( 'All Opportunities', 'dac' ),
		'parent_item'                => __( 'Parent Opportunity', 'dac' ),
		'parent_item_colon'          => __( 'Parent Opportunity:', 'dac' ),
		'new_item_name'              => __( 'New Opportunity Name', 'dac' ),
		'add_new_item'               => __( 'Add New Opportunity', 'dac' ),
		'edit_item'                  => __( 'Edit Opportunity', 'dac' ),
		'update_item'                => __( 'Update Opportunity', 'dac' ),
		'view_item'                  => __( 'View Opportunity', 'dac' ),
		'separate_items_with_commas' => __( 'Separate Opportunities with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove Opportunities', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Opportunities', 'dac' ),
		'search_items'               => __( 'Search Opportunities', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No Opportunities', 'dac' ),
		'items_list'                 => __( 'Opportunities list', 'dac' ),
		'items_list_navigation'      => __( 'Opportunities list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'opportunity', array( 'youth-opportunity' ), $args );

}
add_action( 'init', 'youth_opportunity_taxonomy', 0 );

// No pagination.
add_filter('pre_get_posts', 'youth_opportunity_pre_get_posts');
function youth_opportunity_pre_get_posts( $query ) {

    if ( !is_admin() && $query->is_post_type_archive( 'youth-opportunity' ) && $query->is_main_query() ) {
        $query->set( 'posts_per_page', '-1' );
    }

    return $query;
}